
<%
if (((String)(session.getAttribute("shouldAllowAccess")))!="true"){
out.print("Please, sign in before going here");
}
else{
%>
<style>

.axis .domain {
  display: none;
}

</style>
<script src="navControls.js"></script>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><img src="https://png.icons8.com/windows/50/ffffff/rocket.png" class="navbarIcon">Apollo 13</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item"  id="navbarDeals">
        <a class="nav-link">Deals</a>
      </li>
      <li class="nav-item active" id="navbarInstruments">
        <a class="nav-link" >Instruments</a>
      </li>
      <li class="nav-item" id="navbarCounterParty">
        <a class="nav-link" >Counter Party</a>
      </li>
      <li class="nav-item" id="navbarDealsTable">
        <a class="nav-link" >Deals Table</a>
      </li>
    </ul>
  </div>
</nav>
<div class="row">
  <div class="col-md-2">
    &nbsp;
  </div>
  <div class="col-md-8 shadowCard">
    <select class="js-example-basic-single" id="instrumentDropDown" name="instrument">
      <option></option>

    </select>
  </div>
  <div class="col-md-2">

  </div>
</div>
<div id="instrumentsGraph">
  <div class="row">
    <div class="col-md-1">
      
    </div>
    <div class="col-md-10">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-3 col-sm-4">
          <div class="wrimagecard wrimagecard-topimage">
            <div class="wrimagecard-topimage_header" style="background-color:rgba(187, 120, 36, 0.1) ">
              <center><i class="fa fa-tag" style="color:#BB7824"></i></center>
            </div>
            <div class="wrimagecard-topimage_title">
              <h6>Open<div class="pull-right badge" id="openCard"></div></h6>
              <h6>Close<div class="pull-right badge" id="closeCard"></div></h6>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4">
          <div class="wrimagecard wrimagecard-topimage">
            <div class="wrimagecard-topimage_header" style="background-color: rgba(22, 160, 133, 0.1)">
              <center><i class = "fa fa-money" style="color:#16A085"></i></center>
            </div>
            <div class="wrimagecard-topimage_title">
              <h6>Average Buy<div class="pull-right badge" id="buyCard"></div></h6>
              <h6>Average Sell<div class="pull-right badge" id="sellCard"></div></h6>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4">
          <div class="wrimagecard wrimagecard-topimage">
            <div class="wrimagecard-topimage_header" style="background-color:  rgba(213, 15, 37, 0.1)">
              <center><i class="fa fa-exchange fa-rotate-90" style="color:#d50f25"> </i></center>
            </div>
            <div class="wrimagecard-topimage_title" >
              <h6>High<div class="pull-right badge" id="highCard"></div></h6>
              <h6>Low<div class="pull-right badge" id="lowCard"></div></h6>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-4">
          <div class="wrimagecard wrimagecard-topimage">
            <div class="wrimagecard-topimage_header" style="background-color:  rgba(51, 105, 232, 0.1)">
             <center><i class="fa fa-percent" style="color:#3369e8"> </i></center>
            </div>
            <div class="wrimagecard-topimage_title">
              <h6>Percent Change<div class="pull-right badge" id="percentChange"></div></h6>
              <h6>Volatility<div class="pull-right badge" id="volatility"></div></h6>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
    <div class="col-md-1">
      
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      &nbsp;
    </div>
    <div class="col-md-8 shadowCard">
      <script src="instrumentLineGraph.js"></script>
      <div id="myDiv"></div>
      <br>
      <div id="myDiv1"></div>
    </div>
    <div class="col-md-2">

    </div>
  </div>
  
</div>

<%
}
%>

