$( document ).ready(function() {
  function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
  }

  $.ajax({
    method: 'GET',
    url: "graphData.jsp",
    datatype: "json",
    success: function(msg) {
      if(msg == "error") {
        console.log("error");
      } else {
        var msg = JSON.parse(msg);
        var instrumentNameArr = [];
        var buyPriceArr = [];
        var sellPriceArr = [];
        for(var i = 0; i < msg.length; i++) {
          instrumentNameArr[i] = msg[i].instrument_name;
          buyPriceArr[i] = msg[i].buy;
          sellPriceArr[i] = msg[i].sell

        }

        var trace1 = {
          x: instrumentNameArr,
          y: buyPriceArr,
          name: 'Buy',
          type: 'bar'
        };

        var trace2 = {
          x: instrumentNameArr,
          y: sellPriceArr,
          name: 'Sell',
          type: 'bar'
        };
        var data = [trace1, trace2];

        var layout = {
          barmode: 'group',
          title: 'Average Price per Instrument',
          titlefont: {
            size: 26,
          }
        };

        Plotly.newPlot('dashboardChart', data, layout);
      }
    },
    error: function(msg) {
      console.log("didnt work");
    }
  });
  $('#rankingsDataTable').DataTable({
    "paging":   false,
    "searching": false,
    "info": false,
    "order": [[ 1, "desc" ]],
    "language": {
      "emptyTable": "Loading data..."
    }
  });
  $('#effectiveDataTable').DataTable({
    "paging":   false,
    "searching": false,
    "info": false,
    "order": [[ 1, "desc" ]],
    "language": {
      "emptyTable": "Loading data..."
    }
  });
  $.ajax({
    method: 'GET',
    url: "realisedProfits.jsp",
    success: function(msg) {
      if(msg == "error") {
        console.log(msg);
      } else {
        var realizedTableData = JSON.parse(msg);
        var t = $('#rankingsDataTable').DataTable();
        t.clear();
        $.each(realizedTableData, function(i, item) {
          var tr = $('<tr>').append(
            $('<td>').text(item.dealer),
            $('<td>').text(formatNumber(item.ending_position.toFixed(0))),
            )
          t.row.add($(tr)).draw();

        }); 
      }
    },
    error: function(msg) {
      console.log("didnt work");
    }
  });
   $("#doneLoading").css("display", "block");
   $.ajax({
    method: 'GET',
    url: "effectiveProfits.jsp",
    success: function(msg) {
      if(msg == "error") {
        console.log(msg);
      } else {
        var realizedTableData = JSON.parse(msg);
        var t = $('#effectiveDataTable').DataTable();
        t.clear();
        $.each(realizedTableData, function(i, item) {
          var tr = $('<tr>').append(
            $('<td>').text(item.counterparty_name),
            $('<td>').text(formatNumber(item.effective_profit.toFixed(0))),
            )
          t.row.add($(tr)).draw();

        }); 
      }
    },
    error: function(msg) {
      console.log("didnt work");
    }
  });
});
